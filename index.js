function drawStrokeRect(x0, y0, x1, y1) {
    let canvas = document.getElementById('myCanvas');
    if (canvas.getContext) {
        let ctx = canvas.getContext("2d");
        ctx.strokeStyle = "green";
        let { width, height } = calculateWidthAndHeight(x0, y0, x1, y1)
        repaintCanvasBg(image)
        ctx.strokeRect(x0, y0, width, height); // x y width height
    }
}
function calculateWidthAndHeight(x0, y0, x1, y1) {
    let width = x1 - x0
    let height = y1 - y0
    return { width: width, height: height }
}
function repaintCanvasBg(image) {
    let canvas = document.getElementById('myCanvas')
    if (canvas.getContext) {
        let ctx = canvas.getContext("2d");
        // let image = document.getElementById(`cat`)
        ctx.drawImage(image, 0, 0, canvas.width, canvas.height);
    }
}
function initCanvasWithImage(image) {
    let canvas = document.getElementById('myCanvas')
    let ctx = canvas.getContext("2d")
    ctx.drawImage(image, 0, 0, canvas.width, canvas.height)

    console.log('init')
}



console.log('lel')
let canvas = document.getElementById("myCanvas")
let image = document.getElementById(`cat`)
let imageContainer = document.getElementById('image-container')

let isDown = false;

let mouseXMove = document.querySelector(`.mouseX_panel_1`)
let mouseYMove = document.querySelector(`.mouseY_panel_1`)

let mouseXClickDown = document.querySelector(`.mouseX_panel_2`)
let mouseYClickDown = document.querySelector(`.mouseY_panel_2`)
let mouseClickDownCoords = {}

let mouseXClickUp = document.querySelector(`.mouseX_panel_3`)
let mouseYClickUp = document.querySelector(`.mouseY_panel_3`)



window.addEventListener('load', () => { initCanvasWithImage(image) })

canvas.addEventListener('mousemove', event => {
    let x1 = event.offsetX
    let y1 = event.offsetY

    mouseXMove.innerText = `x = ${event.offsetX}`
    mouseYMove.innerText = `y = ${event.offsetY}`
    if (isDown) {
        console.log(`Я вожу с зажатой кнопкой`)
        let { x0, y0 } = mouseClickDownCoords
        drawStrokeRect(x0, y0, x1, y1)
    }
})

canvas.addEventListener(`mousedown`, event => {
    mouseXClickDown.innerText = `x = ${event.offsetX}`
    mouseYClickDown.innerText = `y = ${event.offsetY}`
    isDown = true
    mouseClickDownCoords.x0 = event.offsetX;
    mouseClickDownCoords.y0 = event.offsetY;
    console.log(`кнопка вниз`)
})

function kek(pickedWidth, pickedHeight, oldCanvas, x0, y0, increaseValue = 0) {
    let newCanvas = document.createElement('canvas');
    newCanvas.id = 'newCanvas'
    // let increaseValue = 0; // коофицент увеличения картинки. дефолтное значение. Для больших исходных картинок норм. Для маленьких картинок лучше использовать увеличение. Когда сделаю выделение оригинальной области, все будет ок
    // let increaseValue = 50
    newCanvas.width = pickedWidth + increaseValue;
    newCanvas.height = pickedHeight + increaseValue;
    let newContext = newCanvas.getContext('2d');
    newContext.drawImage(oldCanvas, x0, y0, pickedWidth, pickedHeight, 0, 0, newCanvas.width, newCanvas.height);
    document.body.appendChild(newCanvas)
    let url = newCanvas.toDataURL(`image/png`, 1.0) // я могу передавать base64 в тессеракт. blob тоже могу. Длина base64 строки в хроме  должна быть <=2mb. Это вроде ограничениие хрома
    console.log(url)


    //преобразование в билоб 
    // let Blob = newCanvas.toBlob((blob) => {
    //     let urlFromBlob = URL.createObjectURL(blob);
    //     console.log(`blob url`)
    //     console.log(urlFromBlob)
    // })
}
canvas.addEventListener(`mouseup`, event => {
    let x1 = event.offsetX
    let y1 = event.offsetY

    mouseXClickUp.innerText = `x = ${event.offsetX}`
    mouseYClickUp.innerText = `y = ${event.offsetY}`
    isDown = false
    console.log(`кнопка вверх`)
    let { x0, y0 } = mouseClickDownCoords
    let { width, height } = calculateWidthAndHeight(x0, y0, x1, y1)
    console.log(`newWidth = ${width}`) // ширина выделенной области
    console.log(`newHeight = ${height}`) //высота выделенной области
    repaintCanvasBg(image)

    kek(width, height, canvas, x0, y0)

})